package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODYTY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST_BR = "支店定義ファイルが存在しません";
	private static final String FILE_NOT_EXIST_COM = "商品定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT_BR = "支店定義ファイルのフォーマットが不正です";
	private static final String FILE_INVALID_FORMAT_COM = "商品定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONSECUTIVE_NUMBER = "売上ファイルが連番になっていません";
	private static final String PROCEEDS_ERROR = "合計金額が10桁を超えました";
	private static final String ILLEGAL_FORMAT = "のフォーマットが不正です";
	private static final String ILLEGAL_CODE_BR = "の支店コードが不正です";
	private static final String ILLEGAL_CODE_COM = "の商品コードが不正です";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// エラー処理3-1 コマンドライン引数が渡されていない
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		String branchCode = "^[0-9]{3}$";
		String commodityCode = "^[A-Za-z0-9]{8}$";

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, FILE_NOT_EXIST_BR, FILE_INVALID_FORMAT_BR, branchCode)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, FILE_NOT_EXIST_COM, FILE_INVALID_FORMAT_COM, commodityCode)) {
			return;
		}


		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		// 処理内容2-1 コマンドライン引数で指定されたフォルダ（ディレクトリ）から
		// 拡張子がrcd、かつファイル名が数字8桁のファイルを検索
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			File rcd = files[i];
			String file = files[i].getName();

			if(rcd.isFile() && file.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// エラー処理2-1 連番か確認
		Collections.sort(rcdFiles);

		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			String rcdDate = rcdFiles.get(i).getName();
			String rcdDate2 = rcdFiles.get(i + 1).getName();

			int former = Integer.parseInt(rcdDate.substring(0,8));
			int latter = Integer.parseInt(rcdDate2.substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_CONSECUTIVE_NUMBER);
				return;
			}
		}

		// ここまでが処理内容2－1、ここから2－2
		// 該当ファイルから、支店コード、商品コード、売上額を抽出
		// 抽出した売上額を該当する支店の合計額、商品の合計額に加算
		// 売上ファイルの数だけ繰り返す

		BufferedReader br = null;

		for(int i = 0; i < rcdFiles.size(); i++) {
			ArrayList<String> contentsFile = new ArrayList<>();

			try {
				File file = (File) rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String rcdFile = rcdFiles.get(i).getName();

				String line;
				while((line = br.readLine()) != null) {

					contentsFile.add(line);
				}

				// エラー処理2-5 フォーマットが不正
				if(contentsFile.size() != 3) {
					System.out.println(rcdFile + ILLEGAL_FORMAT);
					return;
				}

				// エラー処理2-3 支店コードが不正
				if(!branchSales.containsKey(contentsFile.get(0))) {
					System.out.println(rcdFile + ILLEGAL_CODE_BR);
					return;
				}

				// エラー処理2-4 商品コードが不正
				if(!commoditySales.containsKey(contentsFile.get(1))) {
					System.out.println(rcdFile + ILLEGAL_CODE_COM);
				}

				// エラー処理3-2 売上金額が数字でない
				if(!contentsFile.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(contentsFile.get(2));
				Long saleAmountBr = branchSales.get(contentsFile.get(0)) + fileSale;
				Long saleAmountCom = commoditySales.get(contentsFile.get(1)) + fileSale;

				// エラー処理2-2 10桁超え
				if(saleAmountBr >= 10000000000L) {
					System.out.println(PROCEEDS_ERROR);
					return;
				}
				if(saleAmountCom >= 10000000000L) {
					System.out.println(PROCEEDS_ERROR);
					return;
				}

				branchSales.put(contentsFile.get(0),saleAmountBr);
				commoditySales.put(contentsFile.get(1), saleAmountCom);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0],FILE_NAME_COMMODYTY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 * @param fileNotExistBr
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String fileNotExist, String fileInvForm, String code) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// エラー処理1-1,1-3 定義ファイルが存在しない
			if(!file.exists()) {
				System.out.println(fileNotExist);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 処理内容1-2 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				// エラー処理1-2,1-4 定義ファイルのフォーマットが不正
				if((items.length != 2) || (!items[0].matches(code))) {
					System.out.println(fileInvForm);
					return false;
				}

				// 支店コードとそれに対応する支店名を保持
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		File file = new File(path,fileName);

		BufferedWriter bw = null;

		try {
			FileWriter fw= new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				String name = names.get(key);
				Long value = sales.get(key);

				bw.write(key + "," + name + "," + value.toString());
				bw.newLine();
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;

		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
